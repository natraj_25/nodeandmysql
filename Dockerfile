FROM node:10-alpine
WORKDIR /app
ADD package.json /app
RUN npm install
COPY . /app
COPY ./src /app/src
CMD [ "npm", "start" ]